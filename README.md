# universis-signer

Universis pdf signing tools

universis-signer is a tiny client-side application which enables pdf signing by using 
either a PKCS12 local keystore or a PKCS11 keystore of a USB token. These signing operations are part of 
[@universis/reports](https://gitlab.com/universis/reports.git) package and allow users to sign student certificates,
transcripts and any other document that requires a digital signature.

## Usage

### Start universis-signer with a local PKCS12 key store:

        java -jar universis-signer-1.0.1.jar -keyStore ./keystore.p12 -storeType PKCS12
    
where `keyStore` arguments holds the path of the local PKCS12 key store.
    
### Start universis-signer with a PKCS11 key store of a USB token:

        java -jar universis-signer-1.0.1.jar -keyStore none -storeType PKCS11 -providerArg ./eToken.cfg
    
where `storeType` argument holds key store type (PKCS11) and `providerArg` contains the path of USB token configuration e.g.

    name=eToken
    library=/usr/local/lib/libeTPkcs11.dylib
    slot=0

or any other USB token required configuration

## List key store certificates

The following request

    curl --location --request GET 'localhost:2465/keystore/certs' \
    --header 'Authorization: Basic dXNlcjpzZWNyZXQ='

returns a list of the available certificates e.g.

    [
        {
            "version": 3,
            "subjectDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
            "sigAlgName": "SHA256withRSA",
            "sigAlgOID": "1.2.840.113549.1.1.11",
            "issuerDN": "CN=Universis Test Signer, OU=Universis Development and Testing, O=Universis Development, L=Thessaloniki, ST=Thessaloniki, C=GR",
            "serialNumber": 11651655752893169577,
            "notAfter": "Jun 18, 2022 9:29:12 AM",
            "notBefore": "Jun 18, 2020 9:29:12 AM",
            "expired": false,
            "thumbprint": "d33ac6cc6290bcfb4a23243af0ec98b4f49b2238",
            "commonName": "Universis Test Signer"
        }
    ]
    
Important note: In basic HTTP authentication, a request contains a header field 
in the form of Authorization: Basic <credentials>, where credentials is the Base64 encoding of ID and password joined by a single colon :

`user` parameter is optional (use a default value like "user") and `password` parameter is the password of the key store.
 

## Sign pdf document

![Signed PDF Document](./screenshot-signed-pdf.png)

The following request passes an input file and returns a signed pdf e.g.

    curl --location --request POST 'localhost:2465/sign/' \
    --header 'Authorization: Basic dXNlcjoxMjM0' \
    --form 'thumbprint=f641d66d49ba43bf911e873889d7fa0e32e26ce6' \
    --form 'position=20,10,320,120' \
    --form 'file=@/home/user/Downloads/document.pdf'

where `thumbprint` parameter is the thumbprint of the certificate that is going to used to sign pdf document,
`position` is an optional parameter of the position of the signature block and `file` contains the pdf document to be signed.

Important note: In basic HTTP authentication, a request contains a header field 
in the form of Authorization: Basic <credentials>, where credentials is the Base64 encoding of ID and password joined by a single colon :

`user` parameter is optional (use a default value like "user") and `password` parameter is the password of the key store.
 
 ## Build from sources
 
Clone universis-signer repository:
 
    git clone https://gitlab.com/universis/universis-signer.git
    
Run maven and build package:

    mvn package


