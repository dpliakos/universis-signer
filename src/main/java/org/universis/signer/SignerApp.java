package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.commons.cli.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class SignerApp extends RouterNanoHTTPD {

    private static final String DEFAULT_HOST = "localhost";
    private static final int DEFAULT_PORT = 2465;

    protected SignerAppConfiguration configuration;

    public SignerApp() throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.addMappings();
    }

    public SignerApp(SignerAppConfiguration configuration) throws IOException {
        super(SignerApp.DEFAULT_HOST, SignerApp.DEFAULT_PORT);
        this.configuration = configuration;
        this.addMappings();
    }

    public static void main(String[] args) {
        try {

            Options options = new Options();

            Option keyStoreOption = new Option("ks", "keyStore", true, "keyStore file path e.g. ./keystore.p12 or none");
            keyStoreOption.setRequired(false);
            options.addOption(keyStoreOption);

            Option storeTypeOption = new Option("st", "storeType", true, "keyStore type e.g. PKCS11 PKCS12");
            storeTypeOption.setRequired(false);
            options.addOption(storeTypeOption);

            Option providerArgOption = new Option("pa", "providerArg", true, "keyStore provider argument e.g ./token.cfg for a PKCS11 key store");
            providerArgOption.setRequired(false);
            options.addOption(providerArgOption);

            CommandLineParser parser = new DefaultParser();
            HelpFormatter formatter = new HelpFormatter();
            CommandLine cmd;

            try {
                cmd = parser.parse(options, args);
            } catch (ParseException e) {
                System.out.println(e.getMessage());
                formatter.printHelp("java -jar universis-signer-x.x.x.jar", options);
                System.exit(1);
                return;
            }
            SignerAppConfiguration configuration = new SignerAppConfiguration();
            configuration.keyStore = cmd.getOptionValue("keyStore");
            configuration.storeType = cmd.getOptionValue("storeType");
            configuration.providerArg = cmd.getOptionValue("providerArg");
            // create server
            SignerApp server = new SignerApp(configuration);
            // and start
            server.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
        } catch (Exception e) {
            System.err.println("Couldn't start signer app:\n" + e);
        }
    }

    @Override
    public void addMappings() {
        super.addMappings();
        addRoute("/", RootHandler.class);
        addRoute("/keystore/certs", KeyStoreHandler.class, this.configuration);
        addRoute("/sign/", SignerHandler.class, this.configuration);
    }

}
