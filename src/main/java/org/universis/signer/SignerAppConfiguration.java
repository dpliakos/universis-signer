package org.universis.signer;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.*;
import java.security.cert.CertificateException;

/**
 *  org.universis.signer.Signer application configuration options
 */
public class SignerAppConfiguration {
    /**
     * A string which represents the type of keystore pkcs11 or pkcs12
     */
    public String storeType = "pkcs12";
    /**
     * A string which contains the path of token configuration
     */
    public String providerArg;
    /**
     * A string which contains the path of a pkcs12 key store
     */
    public String keyStore;
    /**
     * A string which contains the keystore password
     */
    public String keyStorePass;

    public KeyStore getKeyStore(String password) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
        KeyStore ks;
        // get pkcs12 store
        if (this.storeType.toLowerCase().equals("pkcs12")) {
            ks = KeyStore.getInstance(this.storeType);
            ks.load(new FileInputStream(this.keyStore), password.toCharArray());
            return ks;
        }
        // get pkcs11 store
        if (this.storeType.toLowerCase().equals("pkcs11")) {
            // add provider if missing
            Provider p = new sun.security.pkcs11.SunPKCS11(this.providerArg);
            if (Security.getProvider(p.getName()) == null) {
                Security.addProvider(p);
            }
            ks = KeyStore.getInstance(this.storeType);
            ks.load(null, password.toCharArray());
            return ks;
        }
        // not implemented
        throw new InvalidParameterException("The specified key store type has not implemented yet.");
    }

}
