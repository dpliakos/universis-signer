package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 400 Bad request error
 */
public class BadRequestHandler extends RouterNanoHTTPD.DefaultHandler {
    public String message = "Bad Request";
    public BadRequestHandler(String message) {
        this.message = message;
    }

    public BadRequestHandler() {
    }
    public String getText() {
        return "<html><body><h3>Error 400: " + this.message + ".</h3></body></html>";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.BAD_REQUEST;
    }
}
