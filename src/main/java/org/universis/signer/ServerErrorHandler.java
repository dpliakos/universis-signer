package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import org.apache.http.util.ExceptionUtils;

import java.util.Arrays;

/**
 * Handles 500 Internal Server Error
 */
public class ServerErrorHandler extends RouterNanoHTTPD.DefaultHandler {

    private String message = "Internal Server Error";
    private String stackTrace = "";
    public ServerErrorHandler(String message) {
        this.message = message;
    }

    public ServerErrorHandler() {
    }

    public ServerErrorHandler(Exception e) {
        this.message = e.getMessage();
        this.stackTrace = Arrays.toString(e.getStackTrace());
    }

    public String getText() {
        return "<html><body><h3>Error 500: " + this.message +  ".</h3>" +
                "<p>" + this.stackTrace + "</p>" +
                "</body></html>";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.FORBIDDEN;
    }
}
