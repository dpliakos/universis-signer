package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;

/**
 * Handles 404 Not Found error
 */
public class NotFoundHandler extends RouterNanoHTTPD.DefaultHandler {
    public String message = "Not Found";
    public NotFoundHandler(String message) {
        this.message = message;
    }

    public NotFoundHandler() {
    }
    public String getText() {
        return "<html><body><h3>Error 404: " + this.message + ".</h3></body></html>";
    }

    public String getMimeType() {
        return "text/html";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        return NanoHTTPD.Response.Status.NOT_FOUND;
    }
}
