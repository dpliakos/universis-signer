package org.universis.signer;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.router.RouterNanoHTTPD;
import com.google.gson.Gson;

import java.io.Serializable;

/**
 * Formats responses to JSON
 */
public class JsonResponseHandler extends RouterNanoHTTPD.DefaultHandler {
    private final Serializable any;
    public JsonResponseHandler(Serializable any) {
        this.any = any;
    }
    public String getText() {
        Gson gson = new Gson();
        return gson.toJson(this.any);
    }

    public String getMimeType() {
        return "application/json";
    }

    public NanoHTTPD.Response.IStatus getStatus() {
        if (this.any == null) {
            return NanoHTTPD.Response.Status.NO_CONTENT;
        }
        return NanoHTTPD.Response.Status.OK;
    }
}
